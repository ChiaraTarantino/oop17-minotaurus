package model.board;

import model.settings.GameSettings;
import utilities.FileManager;

/**
 *
 * Andrea Serafini.
 *
 */
public class GameManager implements GameManagerInterface {

    private static GameManager manager;
    private final FileManager<Board> fileManager = new FileManager<>(GameSettings.getSavedGameFileName());
    private Board game;

    /**
     * Delete settings file.
     */
    @Override
    public void deleteSavedGame() {
        fileManager.deleteFile();
    }

    /**
     *
     * @return the saved game
     */
    @Override
    public Board getGame() {
        this.loadSavedGame();
        return this.game;
    }

    /**
     *
     * @return true if the game file is present
     */
    @Override
    public boolean isPresent() {
        return fileManager.isPresent();
    }

    /**
     * Load saved status of a previous game.
     */
    @Override
    public void loadSavedGame() {
        fileManager.loadFile();
        this.game = fileManager.get();
    }

    /**
     * Save the current game status on a file.
     */
    @Override
    public void saveGame() {
        this.game = Board.getLog();
        fileManager.saveFile(game);
    }

    /**
     *
     * @return a gameManager instance
     */
    public static synchronized GameManager getLog() {
        if (manager == null) {
            manager = new GameManager();
        }
        return manager;
    }


}
