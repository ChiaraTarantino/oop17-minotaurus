package model.board;

/**
 * Andrea Serafini.
 *
 */
public interface GameManagerInterface {

    /**
     * Delete settings file.
     */
    void deleteSavedGame();

    /**
     *
     * @return the settings for the game
     */
    Board getGame();

    /**
     *
     * @return true if the settings file is present
     */
    boolean isPresent();

    /**
     * Load saved settings from a previous game.
     */
    void loadSavedGame();

    /**
     * Save the current setting on a file.
     */
    void saveGame();

}
