package model.players;

import utilities.Colors;

/**
 *
 * Andrea Serafini.
 *
 */
public class Player implements User {

    /**
     *
     */
    private static final long serialVersionUID = -7610886613845823536L;
    private String name;
    private Colors color;
    private int turn;

    /**
     * Constructor.
     *
     * @param name
     *            the name of the player
     * @param color
     *            the color of the player
     */
    public Player(final String name, final Colors color) {
        this.name = name;
        this.color = color;
        this.turn = 0;
    }

    /**
     *
     * @param name
     *            the name of the player
     * @param color
     *            the color of the player
     * @param turn
     *            the number of turn the player have played
     */
    public Player(final String name, final Colors color, final int turn) {
        this.name = name;
        this.color = color;
        this.turn = turn;
    }

    /**
     * Change the color of the player.
     */
    @Override
    public void changeColor(final Colors color) {
        this.color = color;
    }

    /**
     * Change the name of the player.
     */
    @Override
    public void changeName(final String name) {
        this.name = name;
    }

    /**
     * Return the name of the player.
     */
    @Override
    public Colors getColor() {
        return this.color;
    }

    /**
     * Return the name of the player.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Return the number of turn played by the player.
     */
    @Override
    public int getTurns() {
        return this.turn;
    }

    /**
     * Increment the number of turn played.
     */
    @Override
    public void incrementTurn() {
        this.turn++;
    }

    /**
     * Reset to zero the number of turn played.
     */
    @Override
    public void resetTurn() {
        this.turn = 0;
    }

    /**
     * Return the string of the player.
     */
    @Override
    public String toString() {
        return /*"Player " + this.color + ", " + */this.name;
    }

}
