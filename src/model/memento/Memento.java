package model.memento;

import javafx.util.Pair;

public interface Memento {
    /**
     * 
     * @param step step to put in the list of steps
     */
    void lastStep(Pair<Integer, Integer> step);
    
    /**
     * 
     * @return the last step 
     */
    Pair<Integer, Integer> getLastStep();
    
    /**
     * 
     * @return true if there is a step, false otherwise
     */
    boolean isStepPresent();
    
    /**
     * 
     */
    void resetMemento();
}
