package model.settings;

import utilities.FileManager;

/**
 *
 * Andrea Serafini.
 *
 */
public class SettingsManager implements SettingsManagerInterface {

    private static SettingsManager manager;
    private final FileManager<GameSettings> savedFileManager = new FileManager<>(GameSettings.getSavedSettingsFileName());
    private final FileManager<GameSettings> fileManager = new FileManager<>(GameSettings.getSettingsFileName());

    private GameSettings settings;

    /**
     * Constructor.
     */
    public SettingsManager() {
        this.loadSettings();
    }

    private void createSettingsFile() {
        this.settings = new GameSettings();
        this.settings.initialize();
        this.fileManager.saveFile(settings);
    }

    /**
     * Delete settings file.
     */
    @Override
    public void deleteSavedSettings() {
       this.savedFileManager.deleteFile();
    }

    /**
     *
     * @return the settings for the game
     */
    @Override
    public GameSettings getSettings() {
        return this.settings;
    }

    /**
     *
     * @return true if the settings file is present
     */
    @Override
    public boolean isPresent() {
        return this.fileManager.isPresent();
    }

    /**
     *
     * @return true if the settings file is present
     */
    @Override
    public boolean isSavedPresent() {
        return this.savedFileManager.isPresent();
    }

    /**
     * Load saved settings from a previous game.
     */
    @Override
    public void loadSavedSettings() {
        if (this.savedFileManager.isPresent()) {
            this.savedFileManager.loadFile();
            this.settings = savedFileManager.get();
        } else {
            this.fileManager.loadFile();
        }
    }

    private void loadSettings() {
        if (this.fileManager.isPresent()) {
            this.fileManager.loadFile();
            this.settings = fileManager.get();
        } else {
            this.createSettingsFile();
        }
    }

    /**
     * Save the current setting on a file.
     */
    @Override
    public void saveSettings() {
        this.settings.decrementTurn();
        this.savedFileManager.saveFile(settings);
    }

    /**
     * Update the saved setting file.
     */
    @Override
    public void updateSavedSettings() {
        if (!this.savedFileManager.isPresent()) {
            this.savedFileManager.saveFile(null);
        }
        this.savedFileManager.updateFile(this.settings);
    }

    /**
    *
    * @return a settingsManager instance
    */
   public static synchronized SettingsManager getLog() {
       if (manager == null) {
           manager = new SettingsManager();
       }
       return manager;
   }
}
