package model.settings;

import java.util.ArrayList;
import java.util.List;

import model.players.Player;

/**
 * Andrea Serafini.
 *
 * Classe di prova in attesa dei settings corretti.
 */
public class TemporaryGameSettings {

    private static TemporaryGameSettings settings;

    private int boardLimit;
    private List<Player> matchPlayers;
    private Integer minotaurusSteps;
    private boolean jumpEnable;
    private boolean edgeJumping;
    private int maze;
    private Integer currentPlayer;
    private boolean minotaurusEdgeJumping;
    private int charactersForWin;

    private boolean musicOn;

    /**
     * Contructor.
     */
    public TemporaryGameSettings() {
        this.boardLimit = 29;
        this.matchPlayers = new ArrayList<>();
        this.edgeJumping = true;
        this.jumpEnable = false;
        this.currentPlayer = -1;
        this.musicOn = true;
        this.charactersForWin = 2;
        this.minotaurusEdgeJumping = false;
        this.minotaurusSteps = 6;
        this.edgeJumping = false;
    }

    public void addNewPlayer(final Player player) {
        this.matchPlayers.add(player);
    }

    public int getBoardLimit() {
        return this.boardLimit;
    }

    public int getCharactersForWin() {
        return this.charactersForWin;
    }

    public List<Player> getMatchPlayers() {
        return this.matchPlayers;
    }

    public int getMaze() {
        return this.maze;
    }

    public Integer getMinotaurusSteps() {
        return this.minotaurusSteps;
    }

    public Player getNextPlayer() {
        this.currentPlayer++;

        if (this.currentPlayer == this.matchPlayers.size()) {
            this.currentPlayer = 0;
        }
        return this.matchPlayers.get(this.currentPlayer);
    }

    public boolean isEdgeJumping() {
        return this.edgeJumping;
    }

    public boolean isMinotaurusHedgeJumping() {
        return this.minotaurusEdgeJumping;
    }

    /**
     * @return the musicOn
     */
    public boolean isMusicOn() {
        return this.musicOn;
    }

    public boolean jumpEnabled() {
        return this.jumpEnable;
    }

    public void resetTurn() {
        this.matchPlayers.stream().forEach(e -> e.resetTurn());
    }

    public void setBoardLimit(final int boardLimit) {
        this.boardLimit = boardLimit;
    }

    public void setCharactersForWin(final Integer value) {
        this.charactersForWin = value;
    }

    public void setEdgeJumping(final boolean isEdgeJumping) {
        this.edgeJumping = isEdgeJumping;
    }

    public void setJumpEnable(final boolean jumpEnable) {
        this.jumpEnable = jumpEnable;
    }

    public void setMatchPlayers(final List<Player> matchPlayers) {
        this.matchPlayers = matchPlayers;
    }

    public void setMaze(final int maze) {
        this.maze = maze;
    }

    public void setMinotaurusHedgeJumpingMode(final boolean isMinotaurusHedgeJumpingMode) {
        this.minotaurusEdgeJumping = isMinotaurusHedgeJumpingMode;
    }

    public void setMinotaurusSteps(final Integer minotaurusSteps) {
        this.minotaurusSteps = minotaurusSteps;
    }

    /**
     * @param musicOn
     *            the musicOn to set
     */
    public void setMusicOn(final boolean musicOn) {
        this.musicOn = musicOn;
    }

    public static synchronized TemporaryGameSettings getLog() {
        if (settings == null) {
            settings = new TemporaryGameSettings();
        }
        return settings;
    }

}
