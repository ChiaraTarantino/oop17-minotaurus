package model.test;

import java.util.List;

import org.junit.Test;

import model.players.Player;
import model.players.ranking.RankingManager;

/**
 *
 * Andrea Serafini.
 *
 */
public class RankingTest {

    @SuppressWarnings("unused")
    private List<Player> scores;

    private final RankingManager score = new RankingManager();

    private void clear() {
        this.score.clear();
    }

    /**
     *
     */
    @Test
    public void clearTest() {
        this.clear();
    }
}
