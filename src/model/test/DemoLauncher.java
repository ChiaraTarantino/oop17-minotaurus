package model.test;

import controller.ControllerImpl;
import model.players.Player;
import utilities.Colors;

/**
 *
 * Andrea Serafini.
 *
 */
public final class DemoLauncher {

    /**
     *
     * @param args
     *            args
     */
    public static void main(final String[] args) {

        ControllerImpl.getLog().setEdgeJumpingMode(true);
        ControllerImpl.getLog().addPlayer(new Player("Andrea", Colors.Blue));
        ControllerImpl.getLog().addPlayer(new Player("Piero", Colors.Red));

        ControllerImpl.getLog().openDemoBoardView();

    }

    private DemoLauncher() {
        // not called
    }

}
