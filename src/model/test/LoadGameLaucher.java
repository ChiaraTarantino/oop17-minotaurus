package model.test;

import controller.ControllerImpl;

/**
 * Andrea Serafini.
 *
 */
public final class LoadGameLaucher {

    /**
     *
     * @param args
     *            args
     */
    public static void main(final String[] args) {

        ControllerImpl.getLog().reopenBoardView();
    }

    private LoadGameLaucher() {
        // not called
    }
}
