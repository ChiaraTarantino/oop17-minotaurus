package utilities;

/**
 *
 * Andrea Serafini.
 *
 */
public enum Directions {
    /**
     * Direction selected.
     */
    UP, DOWN, LEFT, RIGHT;
}
