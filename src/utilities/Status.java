package utilities;

/**
 *
 * Andrea Serafini.
 *
 */
public enum Status {
    /**
     * Tipologia di parte fissa del labirinto.
     */
    SIEPE, TEMPIO, ARRIVO, PARTENZA, VUOTO;
}
