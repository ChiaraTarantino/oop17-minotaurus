package utilities;

/**
 *
 * Andrea Serafini.
 *
 */
public enum Elements {
    /**
     * Eroe, personaggio principale di ogni giocatore.
     */
    EROE,
    /**
     * Minotauro, personaggio comandabile da ogni giocatore.
     */
    MINOTAURO,
    /**
     * Muro, parte mobile del labirinto.
     */
    MURO;
}
