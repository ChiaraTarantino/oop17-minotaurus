package controller;

import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

import controller.audio.Audio;
import controller.audio.AudioManager;
import controller.audio.AudioManagerImpl;
import javafx.util.Pair;
import model.board.Board;
import model.board.BoardInterface;
import model.board.GameManager;
import model.board.Initializator;
import model.dice.Dice;
import model.dice.DiceFactory;
import model.memento.Memento;
import model.memento.MementoImpl;
import model.movement.Movement;
import model.movement.MovementManager;
import model.players.Player;
import model.players.ranking.Ranking;
import model.players.ranking.RankingManager;
import model.settings.SettingsManager;
import model.settings.SettingsManagerInterface;
import utilities.Colors;
import utilities.Directions;
import utilities.Elements;
import utilities.Status;
import view.board.BoardView;
import view.board.BoardViewInterface;
import view.board.DemoBoardView;
import view.board.WinDialog;
import view.board.WinDialogInterface;

/**
 *
 * Piero Sanchi. This class creates one instance of the controller.
 *
 */
public class ControllerImpl implements Controller {

    private static final String MINOTAURO = "minotauro";
    private static final String MURO = "muro";
    private static final String EROE = "eroe";
    private static final String JUMP = "jump";

    private BoardInterface gameBoard;
    private Movement movementManager;
    private BoardViewInterface boardView;
    private WinDialogInterface win;
    private Pair<Integer, Integer> selected = null;
    private Pair<Integer, Integer> oldSelected = null;
    private Player actualPlayer; // l'interfaccia di player è user ma non implementa tutti i metodi
    private Dice actualDice;
    private int steps;
    private int arrivedHero = 0;
    private Elements actualElementType;
    private Directions direction;
    private final Memento memento = new MementoImpl();
    private final DiceFactory diceFactory = new DiceFactory();
    private final AudioManager audio = new AudioManagerImpl();
    private final SettingsManagerInterface settings = SettingsManager.getLog();
    private final Ranking rank = new RankingManager();

    private static ControllerImpl controller;

    public static synchronized Controller getLog() {
        if (controller == null) {
            controller = new ControllerImpl();
        }
        return controller;
    }

    @Override
    public void addPlayer(final Player player) {
        this.settings.getSettings().addNewPlayer(player);
    }

    private boolean arrivedEnoughHero() {
        this.gameBoard.getHeroMap().values().iterator().forEachRemaining(e -> {
            if (e.getColor().equals(this.actualPlayer.getColor()) && e.isArrived()) {
                this.arrivedHero++;
            }
        });
        if (this.settings.getSettings().getHeroForWin() == this.arrivedHero) {
            this.arrivedHero = 0;
            return true;
        } else {
            this.arrivedHero = 0;
            return false;
        }
    }

    @Override
    public void deleteSavedGame() {
        this.settings.deleteSavedSettings();
        GameManager.getLog().deleteSavedGame();
    }

    private void endGame() {
        this.rank.addScore(this.actualPlayer);
        this.win = new WinDialog();
        this.boardView.close();
        this.win.setRank(this.rank.getHighscoreString());
        this.audio.play(Audio.WIN);
    }

    private void endTurn() {
        this.selected = null;
        this.setActualPlayer();
        this.boardView.getSidePanel().repaintLabel(this.actualPlayer.getColor());
        this.boardView.waitDice();
        this.actualPlayer.incrementTurn();
        this.memento.resetMemento();
        this.settings.getSettings().setJumpEnabled(false);
        // setto grafica per attesa dado
    }

    @Override
    public int getCharactersForWin() {
        return this.settings.getSettings().getHeroForWin();
    }

    @Override
    public int getMinotaurusSteps() {
        return this.settings.getSettings().getMinotaurusSteps();
    }

    @Override
    public String getRanking() {
        return this.rank.getHighscoreString();
    }

    @Override
    public int getSelectedMaze() {
        return this.settings.getSettings().getMaze();
    }

    @Override
    public boolean isHedgeJumpingMode() {
        return this.settings.getSettings().isHedgeJumping();
    }

    @Override
    public boolean isMinotaurusHedgeJumpingMode() {
        return this.settings.getSettings().isMinotaurusHedgeJumping();
    }

    @Override
    public boolean isNotLastStep() {
        return this.steps != 1;
    }

    @Override
    public void openBoardView() {
        this.deleteSavedGame();
        this.gameBoard = Board.getLog();
        this.boardView = new BoardView(this.settings.getSettings().getBoardLimit() + 1,
                this.settings.getSettings().getBoardLimit() + 1);// makes new ButtonGrid with 2 parameters
        this.prepareBoardView();
    }

    @Override
    public void openDemoBoardView() {
        this.gameBoard = Board.getLog();
        this.boardView = new DemoBoardView(this.settings.getSettings().getBoardLimit() + 1,
                this.settings.getSettings().getBoardLimit() + 1);// makes new ButtonGrid with 2 parameters
        this.prepareBoardView();
    }

    private void prepareBoardView() {
        this.movementManager = new MovementManager();
        this.actualDice = this.diceFactory.getDice(this.settings.getSettings().isHedgeJumping());
        this.setActualPlayer();
        this.boardView.waitDice();
        this.boardView.getSidePanel().repaintLabel(this.actualPlayer.getColor());
        this.actualPlayer.incrementTurn();
        this.updateAll();
        this.boardView.show();
    }

    private void redrawBoard() {
        if (this.gameBoard.getMinotaurus().isEating()) {
            this.audio.play(Audio.EVIL);
            this.updateOldBackground();
            this.movementManager.resetHero(this.selected);
            this.oldSelected = this.selected;
            this.updateOldBackground();
            this.gameBoard.getMinotaurus().restartMinotaurus();
            this.gameBoard.getMinotaurus().setIsEating();
            this.select(null);
            this.updateElements();
            this.endTurn();
        } else if (this.gameBoard.getWallMap().containsKey(this.selected)) {
            this.updateOneBackground(this.gameBoard.getWallMap().get(this.selected).getOldActualPosition());
            this.updateOneBackground(this.gameBoard.getWallMap().get(this.selected).getOldSecondPosition());
            this.updateOneElement();
            this.updateWall(this.gameBoard.getWallMap().get(this.selected).getSecondPosition());
        } else {
            this.updateOldBackground();
            this.updateOneElement();
        }
    }

    @Override
    public void reopenBoardView() {
        Board.restoreBoard();
        SettingsManager.getLog().loadSavedSettings();
        this.gameBoard = Board.getLog();
        this.boardView = new BoardView(this.settings.getSettings().getBoardLimit() + 1,
                this.settings.getSettings().getBoardLimit() + 1);// makes new ButtonGrid with 2 parameters
        this.prepareBoardView();
        this.deleteSavedGame();
    }

    @Override
    public void reset() {
        Initializator.getLog().restoreBoard();
        this.selected = null;
        Board.resetBoard();
        this.gameBoard.resetArrivedHero();
        this.settings.getSettings().resetTurn();
        this.openBoardView();
    }

    @Override
    public void rollDice() {
        this.audio.play(Audio.DICE);
        this.steps = this.actualDice.rollAndGetResult();
        this.boardView.getSidePanel().setImagePath(this.actualDice.getResultFacePath());
        this.boardView.getSidePanel().redrawLabel(EROE);
        switch (this.steps) {
        case 1:
            this.actualElementType = Elements.MINOTAURO;
            this.boardView.getSidePanel().redrawLabel(MINOTAURO);
            this.boardView.getSidePanel().setAvailableNStepsText(this.settings.getSettings().getMinotaurusSteps());
            this.steps = this.settings.getSettings().getMinotaurusSteps(); // setto i passi del minotauro
            this.boardView.waitSelect(this.gameBoard.getMinotaurus().getActualPosition());
            break;
        case 2:
            this.actualElementType = Elements.MURO;
            this.boardView.getSidePanel().redrawLabel(MURO);
            this.boardView.getSidePanel().setAvailableNStepsText(null);
            this.gameBoard.getWallMap().keySet().stream().forEach(e -> {
                this.boardView.waitSelect(e);
            });
            break;
        case 3:
            if (this.settings.getSettings().isHedgeJumping()) {
                this.settings.getSettings().setJumpEnabled(true);
                this.boardView.getSidePanel().redrawLabel(JUMP);
            }
        default:
            this.actualElementType = Elements.EROE;
            this.boardView.getSidePanel().setAvailableNStepsText(this.steps);
            this.gameBoard.getHeroMap().keySet().stream().forEach(e -> {
                if ((this.gameBoard.getHeroMap().get(e).getColor() == this.actualPlayer.getColor())
                        && this.gameBoard.getHeroMap().get(e).isMovable()
                        && !this.gameBoard.getHeroMap().get(e).isArrived()) {
                    this.boardView.waitSelect(e);
                }
            });
            break;
        }
    }

    @Override
    public boolean savedGamePresent() {
        return (this.settings.isSavedPresent() && GameManager.getLog().isPresent());
    }

    @Override
    public void saveGame() {
        if (this.actualElementType == Elements.MURO) {
            this.settings.getSettings().getNextPlayer();
        } else {
            while (this.memento.isStepPresent()) {
                this.undo();
            }
        }
        SettingsManager.getLog().saveSettings();
        GameManager.getLog().saveGame();
    }

    @Override
    public void select(final Pair<Integer, Integer> coord) {
        if (this.gameBoard.getElementAtPosition(coord) != null) {
            this.selected = coord;
            this.boardView.waitMove();
            if (this.gameBoard.getElementAtPosition(coord).getType() == Elements.MURO) {
                this.boardView.getSidePanel().waitSelect();
            } else if (this.gameBoard.getElementAtPosition(coord).getType() == Elements.EROE) {
                this.boardView.selectedIcon(coord);
            }
        }
    }

    private void setActualPlayer() {
        this.actualPlayer = this.settings.getSettings().getNextPlayer();
        this.boardView.getSidePanel().setTurnPlayerText(this.actualPlayer.toString());
        this.boardView.getSidePanel().setHeroArrived(this.gameBoard.arrivedHero(this.actualPlayer),
                this.settings.getSettings().getHeroForWin());
    }

    @Override
    public void setCharactersForWin(final int value) {
        this.settings.getSettings().setHeroForWin(value);
    }

    @Override
    public void setEdgeJumpingMode(final boolean isHedgeJumpingMode) {
        this.settings.getSettings().setHedgeJumping(isHedgeJumpingMode);
    }

    @Override
    public void setMaze(final int maze) {
        this.settings.getSettings().setMaze(maze);
    }

    @Override
    public void setMinotaurusEdgeJumpingMode(final boolean isMinotaurusHedgeJumpingMode) {
        this.settings.getSettings().setMinotaurusHedgeJumping(isMinotaurusHedgeJumpingMode);
    }

    @Override
    public void setMinotaurusSteps(final int minotaurusSteps) {
        this.settings.getSettings().setMinotaurusSteps(minotaurusSteps);
    }

    @Override
    public void startDemoKeyboardListener() {
        final KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(e -> {
            // oldSelected = null;
            if ((e.getID() == KeyEvent.KEY_RELEASED) && (ControllerImpl.this.selected != null)) {
                ControllerImpl.this.oldSelected = ControllerImpl.this.selected;
                switch (e.getKeyCode()) {
                case KeyEvent.VK_DOWN:
                    ControllerImpl.this.direction = Directions.DOWN;
                    break;
                case KeyEvent.VK_UP:
                    ControllerImpl.this.direction = Directions.UP;
                    break;
                case KeyEvent.VK_LEFT:
                    ControllerImpl.this.direction = Directions.LEFT;
                    break;
                case KeyEvent.VK_RIGHT:
                    ControllerImpl.this.direction = Directions.RIGHT;
                    break;
                case KeyEvent.VK_ENTER:
                    ControllerImpl.this.endTurn();
                    break;
                case KeyEvent.VK_R:
                    if (ControllerImpl.this.movementManager.rotate(ControllerImpl.this.selected)) {
                        ControllerImpl.this.updateOneBackground(ControllerImpl.this.gameBoard.getWallMap()
                                .get(ControllerImpl.this.selected).getOldActualPosition());
                        ControllerImpl.this.updateOneElement();
                        ControllerImpl.this.audio.play(Audio.WALL);
                    }
                    break;
                default:
                    break;
                }

                if (((e.getKeyCode() == KeyEvent.VK_DOWN) || (e.getKeyCode() == KeyEvent.VK_UP)
                        || (e.getKeyCode() == KeyEvent.VK_RIGHT) || (e.getKeyCode() == KeyEvent.VK_LEFT))
                        && (ControllerImpl.this.movementManager.move(ControllerImpl.this.selected,
                                ControllerImpl.this.direction))) {
                    ControllerImpl.this.redrawBoard();
                    ControllerImpl.this.memento.lastStep(ControllerImpl.this.oldSelected);
                    if (ControllerImpl.this.actualElementType != Elements.MURO) {
                        ControllerImpl.this.audio.play(Audio.STEP);
                        ControllerImpl.this.steps--;
                        ControllerImpl.this.boardView.getSidePanel().setAvailableNStepsText(ControllerImpl.this.steps);
                    } else {
                        ControllerImpl.this.audio.play(Audio.WALL);
                    }
                    if ((ControllerImpl.this.actualElementType == Elements.EROE) && (ControllerImpl.this.gameBoard
                            .getHeroMap().get(ControllerImpl.this.selected).isArrived())) {
                        ControllerImpl.this.audio.play(Audio.TADA);
                        ControllerImpl.this.steps = 0;
                        if (ControllerImpl.this.arrivedEnoughHero()) {
                            ControllerImpl.this.endGame();
                        } else {
                            ControllerImpl.this.endTurn();
                        }
                    }
                }
            }
            return true;
        });
    }

    @Override
    public void startKeyboardListener() {
        final KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(e -> {
            if ((e.getID() == KeyEvent.KEY_RELEASED) && (ControllerImpl.this.selected != null)) {
                ControllerImpl.this.oldSelected = ControllerImpl.this.selected;
                switch (e.getKeyCode()) {
                case KeyEvent.VK_DOWN:
                    ControllerImpl.this.direction = Directions.DOWN;
                    break;
                case KeyEvent.VK_UP:
                    ControllerImpl.this.direction = Directions.UP;
                    break;
                case KeyEvent.VK_LEFT:
                    ControllerImpl.this.direction = Directions.LEFT;
                    break;
                case KeyEvent.VK_RIGHT:
                    ControllerImpl.this.direction = Directions.RIGHT;
                    break;
                case KeyEvent.VK_ENTER:
                    if ((ControllerImpl.this.steps == 0) || (ControllerImpl.this.actualElementType == Elements.MURO)) {
                        ControllerImpl.this.endTurn();
                    }
                    break;
                case KeyEvent.VK_R:
                    if (ControllerImpl.this.movementManager.rotate(ControllerImpl.this.selected)) {
                        ControllerImpl.this.updateOneBackground(ControllerImpl.this.gameBoard.getWallMap()
                                .get(ControllerImpl.this.selected).getOldActualPosition());
                        ControllerImpl.this.updateOneElement();
                        ControllerImpl.this.audio.play(Audio.WALL);
                    }
                    break;
                default:
                    break;
                }

                if ((ControllerImpl.this.steps != 0)
                        && ((e.getKeyCode() == KeyEvent.VK_DOWN) || (e.getKeyCode() == KeyEvent.VK_UP)
                                || (e.getKeyCode() == KeyEvent.VK_RIGHT) || (e.getKeyCode() == KeyEvent.VK_LEFT))
                        && (ControllerImpl.this.movementManager.move(ControllerImpl.this.selected,
                                ControllerImpl.this.direction))) {
                    ControllerImpl.this.redrawBoard();
                    ControllerImpl.this.memento.lastStep(ControllerImpl.this.oldSelected);
                    if (ControllerImpl.this.actualElementType != Elements.MURO) {
                        ControllerImpl.this.audio.play(Audio.STEP);
                        ControllerImpl.this.steps--;
                        ControllerImpl.this.boardView.getSidePanel().setAvailableNStepsText(ControllerImpl.this.steps);
                    } else {
                        ControllerImpl.this.audio.play(Audio.WALL);
                    }
                    if ((ControllerImpl.this.actualElementType == Elements.EROE) && (ControllerImpl.this.gameBoard
                            .getHeroMap().get(ControllerImpl.this.selected).isArrived())) {
                        ControllerImpl.this.audio.play(Audio.TADA);
                        ControllerImpl.this.steps = 0;
                        if (ControllerImpl.this.arrivedEnoughHero()) {
                            ControllerImpl.this.endGame();
                        } else {
                            ControllerImpl.this.endTurn();
                        }

                    }
                }
            }
            return true;
        });
    }

    @Override
    public void stopWin() {
        this.audio.stop(Audio.WIN);
    }

    @Override
    public void undo() {
        if (this.memento.isStepPresent()) {
            this.steps++;
            this.boardView.getSidePanel().setAvailableNStepsText(this.steps);
            this.oldSelected = this.selected;
            this.movementManager.moveTo(this.selected, this.memento.getLastStep());
            this.redrawBoard();
        }
    }

    private void updateAll() {
        this.updateBackground();
        this.updateElements();
    }

    private void updateBackground() {
        this.gameBoard.getOccupationList().keySet().stream().forEach(e -> {
            this.boardView.drawBackground(e, this.gameBoard.getOccupationList().get(e));
        });
    }

    @Override
    public void updateElements() {
        this.gameBoard.getHeroMap().keySet().stream().forEach(e -> {
            this.boardView.drawElement(e, this.gameBoard.getHeroMap().get(e));
        });
        this.gameBoard.getWallMap().keySet().stream().forEach(e -> {
            this.boardView.drawElement(e, this.gameBoard.getWallMap().get(e));
        });
        this.boardView.drawElement(this.gameBoard.getMinotaurus().getActualPosition(), this.gameBoard.getMinotaurus());
    }

    private void updateOldBackground() {
        if (this.gameBoard.getOccupationList().containsKey(this.oldSelected)) {
            this.boardView.drawBackground(this.oldSelected, this.gameBoard.getOccupationList().get(this.oldSelected));
        } else {
            this.boardView.drawBackground(this.oldSelected, new Pair<>(Status.VUOTO, Colors.Cyan));
        }
    }

    private void updateOneBackground(final Pair<Integer, Integer> coord) {
        this.boardView.drawBackground(coord, new Pair<>(Status.VUOTO, Colors.Cyan));
    }

    private void updateOneElement() {
        this.boardView.drawElement(this.selected, this.gameBoard.getElementAtPosition(this.selected));
    }

    private void updateWall(final Pair<Integer, Integer> coord) {
        this.boardView.drawElement(coord, this.gameBoard.getElementAtPosition(coord));
    }

}
