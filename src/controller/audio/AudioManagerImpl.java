package controller.audio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import model.settings.SettingsManager;

public class AudioManagerImpl implements AudioManager {

    // public static AudioManager audioManager;
    private final SettingsManager settings = SettingsManager.getLog();
    private Clip stepClip;
    private Clip evilClip;
    private Clip tadaClip;
    private Clip winClip;
    private Clip wallClip;
    private Clip diceClip;

    // public static synchronized AudioManager getlog() {
    // if (audioManager == null) {
    // audioManager = new AudioManagerImpl();
    // }
    // return audioManager;
    // }

    // File step = new File("res/audio/step.wav");
    // File evil = new File("res/audio/evil.wav");
    // File tada = new File("res/audio/tada.wav");
    // File win = new File("res/audio/winTheme.wav");
    // File wall = new File("res/audio/wall.wav");
    // File dice = new File("res/audio/dice.wav");

    // AudioStream stepSound;
    // AudioStream evilSound;
    // AudioStream tadaSound;
    // AudioStream winSound;
    // AudioStream wallSound;
    // AudioStream diceSound;

    public AudioManagerImpl() {
        // settings.setMusicOn(true);

        try {

            this.stepClip = AudioSystem.getClip();
            this.evilClip = AudioSystem.getClip();
            this.tadaClip = AudioSystem.getClip();
            this.winClip = AudioSystem.getClip();
            this.wallClip = AudioSystem.getClip();
            this.diceClip = AudioSystem.getClip();

            this.stepClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/step.wav"))));
            this.evilClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/evil.wav"))));
            this.tadaClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/tada.wav"))));
            this.winClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/winTheme.wav"))));
            this.wallClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/wall.wav"))));
            this.diceClip.open(AudioSystem.getAudioInputStream(
                    new BufferedInputStream(this.getClass().getResourceAsStream("/audio/dice.wav"))));
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // stepSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/step.wav"));
        // evilSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/evil.wav"));
        // tadaSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/tada.wav"));
        // winSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/winTheme.wav"));
        // wallSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/wall.wav"));
        // diceSound = new
        // AudioStream(this.getClass().getResourceAsStream("/audio/dice.wav"));

    }

    @Override
    public boolean isAudio() {
        return this.settings.getSettings().isMusicOn();
    }

    @Override
    public void play(final Audio audio) {
        // switch (audio) {
        // case STEP:
        // this.startClip(this.stepClip);
        // break;
        // case DICE:
        // this.startClip(this.diceClip);
        // break;
        // case EVIL:
        // this.startClip(this.evilClip);
        // break;
        // case TADA:
        // this.startClip(this.tadaClip);
        // break;
        // case WALL:
        // this.startClip(this.wallClip);
        // break;
        // case WIN:
        // this.startClip(this.winClip);
        // break;
        // default:
        // break;
        // }
        this.startClip(this.whatAudio(audio));
    }

    @Override
    public void stop(final Audio audio) {
        // switch (audio) {
        // case STEP:
        // this.stopClip(this.stepClip);
        // break;
        // case DICE:
        // this.stopClip(this.diceClip);
        // break;
        // case EVIL:
        // this.stopClip(this.evilClip);
        // break;
        // case TADA:
        // this.stopClip(this.tadaClip);
        // break;
        // case WALL:
        // this.stopClip(this.wallClip);
        // break;
        // case WIN:
        // this.stopClip(this.winClip);
        // break;
        // default:
        // break;
        // }
        this.stopClip(this.whatAudio(audio));
    }

    private Clip whatAudio(final Audio audio) {
        switch (audio) {
        case DICE:
            return this.diceClip;
        case EVIL:
            return this.evilClip;
        case TADA:
            return this.tadaClip;
        case WALL:
            return this.wallClip;
        case WIN:
            return this.winClip;
        default:
            return this.stepClip;
        }
    }

    @Override
    public void setAudio(final Boolean b) {
        this.settings.getSettings().setMusicOn(b);
    }

    private void startClip(final Clip clip) {
        if (this.settings.getSettings().isMusicOn()) {
            clip.setFramePosition(0);
            clip.start();
        }
    }

    private void stopClip(final Clip clip) {
        clip.stop();
        // clip.close();
    }

}
