package controller.audio;

public enum Audio {
    
        STEP,
        WIN,
        EVIL,
        TADA,
        WALL,
        DICE
}
