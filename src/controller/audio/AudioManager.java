package controller.audio;

public interface AudioManager {

    void play(Audio audio);

    void setAudio(Boolean b);

    boolean isAudio();

    void stop(Audio audio);

}
