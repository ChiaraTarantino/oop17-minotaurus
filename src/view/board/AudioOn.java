package view.board;

import controller.audio.AudioManager;
import controller.audio.AudioManagerImpl;

public class AudioOn implements AudioState {

    private static final String AUDIOOFF = "AUDIO: OFF";
    private final AudioManager audioManager = new AudioManagerImpl();

    @Override
    public void execute(GameView gameview) {
        audioManager.setAudio(false);
        gameview.setAudioText(AUDIOOFF);
        gameview.setState(new AudioOff());
    }

}
