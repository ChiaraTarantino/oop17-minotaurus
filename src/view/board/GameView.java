package view.board;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import controller.ControllerImpl;
import utilities.Colors;

/**
 *
 * Piero Sanchi. This class is a side panel for the BoardView.
 *
 */
public class GameView extends JPanel implements GameViewInterface {
    private static final long serialVersionUID = 4800631179056854141L;

    private static final int WIDTH = 350;
    private static final int HEIGHT = 450;
    private static final int TITLEFONT = 17;
    private static final int CONTENTFONT = 16;

    private final JPanel northPanel = new JPanel();
    private final JPanel midPanel = new JPanel();
    private final JPanel southPanel = new JPanel();

    private final JPanel northUpPanel = new JPanel();
    private final JPanel northDownPanel = new JPanel();

    private final JPanel southUpPanel = new JPanel();
    private final JPanel southDownPanel = new JPanel();

    private final JButton diceRoll = new MyButton("Lancia il dado");
    private final JButton audio = new MyButton("AUDIO: ON");
    private final JButton undo = new MyButton("UNDO");
    private final JLabel instructions = new JLabel();
    private final JLabel turn = new JLabel("Turno:");
    private final JLabel turnPlayer = new JLabel();
    private final JLabel availableSteps = new JLabel("Passi disponibili: ");
    private final JLabel availableNSteps = new JLabel();
    private final JLabel lblDice = new JLabel();
    private final JLabel lblEroiAlTempio = new JLabel("Eroi al tempio:");
    private final JLabel lblArrived = new JLabel();
    private final Border border = new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0));
    private final Border downBorder = new EmptyBorder(0, 8, 30, 0);
    private final Border padding = new EmptyBorder(20, 10, 20, 10);
    private final Border instructionPadding = BorderFactory.createEmptyBorder(10, 10, 10, 10);

    private AudioState aState;

    /**
     * GameView Constructor.
     */
    public GameView() {
        super();

        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        this.setBorder(this.padding);

        // operazioni sui bottoni:
        // diceroll
        this.diceRoll.addActionListener(e -> ControllerImpl.getLog().rollDice());

        // undo
        this.undo.addActionListener(e -> ControllerImpl.getLog().undo());

        // audio
        this.setState(new AudioOn());
        this.audio.addActionListener(e -> {
            this.aState.execute(this);
            // if (audioManager.isAudio()) {
            // audioManager.setAudio(false);
            // GameView.this.audio.setText(AUDIOOFF);
            // } else {
            // audioManager.setAudio(true);
            // GameView.this.audio.setText(AUDIOON);
            // }
        });

        // operazioni sulle label
        this.instructions.setBackground(UIManager.getColor("Button.darkShadow"));
        this.instructions.setBorder(BorderFactory.createCompoundBorder(this.border, this.instructionPadding));
        this.instructions.setVerticalAlignment(SwingConstants.TOP);
        this.applyLabelStyle(this.instructions);
        this.turnPlayer.setFont(new Font("Tahoma", Font.BOLD, CONTENTFONT));
        this.setDiceImage("/dice/GREY.jpg");
        this.applyLabelStyle(this.turn);
        this.applyLabelStyle(this.availableSteps);
        this.applyLabelStyle(this.lblEroiAlTempio);
        this.applyContentStyle(this.availableNSteps);
        this.applyContentStyle(this.lblArrived);

        // operazioni su pannelli interni
        this.northUpPanel.add(this.lblDice, BorderLayout.CENTER);
        this.northDownPanel.add(this.diceRoll, BorderLayout.CENTER);
        this.southUpPanel.setLayout(new GridLayout(3, 2, 0, 20));
        this.southUpPanel.add(this.turn);
        this.southUpPanel.add(this.turnPlayer);
        this.southUpPanel.add(this.availableSteps);
        this.southUpPanel.add(this.availableNSteps);
        this.southUpPanel.add(this.lblEroiAlTempio);
        this.southUpPanel.add(this.lblArrived);
        this.southUpPanel.setBorder(this.downBorder);
        this.southDownPanel.add(this.undo);
        this.southDownPanel.add(this.audio);

        // operazioni su pannelli intermedi
        this.northPanel.setLayout(new BoxLayout(this.northPanel, BoxLayout.Y_AXIS));
        this.southPanel.setLayout(new BoxLayout(this.southPanel, BoxLayout.Y_AXIS));
        this.northPanel.add(this.northUpPanel);
        this.northPanel.add(this.northDownPanel);
        this.midPanel.add(this.instructions);
        this.southPanel.add(this.southUpPanel);
        this.southPanel.add(this.southDownPanel);

        // operazioni su pannello esterno
        this.setLayout(new BorderLayout());
        this.add(this.northPanel, BorderLayout.NORTH);
        this.add(this.midPanel);
        this.add(this.southPanel, BorderLayout.SOUTH);

    }

    /**
     * method to apply a style to the label parameter.
     *
     * @param label
     *            the label to apply style to
     */
    private void applyContentStyle(final JLabel label) {
        label.setForeground(Color.BLACK);
        label.setFont(new Font("Tahoma", Font.PLAIN, CONTENTFONT));
    }

    /**
     * method to apply a style to the label parameter.
     *
     * @param label
     *            the label to apply style to
     */
    public void applyLabelStyle(final JLabel label) {
        label.setForeground(Color.BLACK);
        label.setFont(new Font("Tahoma", Font.BOLD, TITLEFONT));
    }

    private void disable(final JButton button) {
        button.setEnabled(false);
        button.setBackground(Color.GRAY);
    }

    private void enable(final JButton button) {
        button.setEnabled(true);
        button.setBackground(Color.BLACK);
    }

    /**
     * method for selecting the instruction too display.
     *
     * @param avviso
     *            the string to select the message
     */
    @Override
    public void redrawLabel(final String avviso) {
        switch (avviso) {
        case "minotauro":
            this.setInstructionsText(
                    "<html>Minotauro:<br>seleziona il minotauro con il mouse <br>e poi spostalo con le frecce <br>direzionali. Per terminare il turno <br>premi INVIO.</html>");
            break;
        case "muro":
            this.setInstructionsText(
                    "<html>Muro: <br>seleziona il muro che vuoi spostare<br>con il mouse, ruotalo con R e <br>spostalo con le frecce direzionali. <br>Per terminare il turno premi INVIO.</html>");
            break;
        case "jump":
            this.setInstructionsText(
                    "<html>Eroe hedgeJump:<br>seleziona l'eroe che vuoi muovere e<br>spostalo con le frecce direzionali.<br>Hai diritto a 3 passi, anche <br>transitando su siepi.</html>");
            break;
        default:
            this.setInstructionsText(
                    "<html>Eroe: <br>seleziona l'eroe che vuoi muovere <br>con il mouse e poi spostalo con le <br>frecce direzionali. Per terminare <br>il turno premi INVIO. </html>");
            break;
        }
    }

    /**
     * method for repaint the turnPlayer label.
     *
     * @param color
     *            the color of the actual player
     */
    @Override
    public void repaintLabel(final Colors color) {
        switch (color) {
        case Blue:
            this.turnPlayer.setForeground(Color.BLUE);
            break;
        case Red:
            this.turnPlayer.setForeground(Color.RED);
            break;
        case White:
            this.turnPlayer.setForeground(Color.DARK_GRAY);
            break;
        case Yellow:
            this.turnPlayer.setForeground(Color.ORANGE);
            break;
        default:
            this.turnPlayer.setForeground(Color.BLACK);
            break;
        }
    }

    @Override
    public void setAudioText(final String text) {
        this.audio.setText(text);
    }

    /**
     * method to write in availableNSteps the number of remaining steps.
     *
     * @param nSteps
     *            to print in GameView
     */
    @Override
    public void setAvailableNStepsText(final Integer nSteps) {
        if (nSteps != null) {
            this.availableNSteps.setText(nSteps.toString());
        } else {
            this.availableNSteps.setText(null);
        }
    }

    /**
     * method for display an image on lblDice.
     *
     * @param imagePath
     *            the path of the image to display
     */
    @Override
    public void setDiceImage(final String imagePath) {
        try {
            this.lblDice.setIcon(new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream(imagePath))));
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * method to write in lblArrived the number of hero arrived.
     *
     * @param arrived
     *            the number of arrived heroes
     * @param requested
     *            the number of heroes arrived to win
     */
    @Override
    public void setHeroArrived(final Integer arrived, final Integer requested) {
        this.lblArrived.setText("\t" + arrived.toString() + " / " + requested.toString());
    }

    /**
     * method for setting the face of the dice to display.
     *
     * @param resultPath
     *            the path of the image to display
     */
    @Override
    public void setImagePath(final String resultPath) {
        this.setDiceImage(resultPath);
    }

    /**
     * method for writing on label instruction the instructions.
     *
     * @param instruction
     *            the instruction message
     */
    @Override
    public void setInstructionsText(final String instruction) {
        this.instructions.setText(instruction);
    }

    @Override
    public void setState(final AudioState state) {
        this.aState = state;
    }

    /**
     * method for writing on turnPlayer the name of the actual player.
     *
     * @param player
     *            the name of the actual player
     */
    @Override
    public void setTurnPlayerText(final String player) {
        this.turnPlayer.setText(player);
    }

    /**
     * method to set the view waiting for the dice.
     */
    @Override
    public void waitDice() {
        this.enable(this.diceRoll);
        this.disable(this.undo);
        this.setInstructionsText("Lancia il dado, Buona fortuna!");
        this.lblDice.setVisible(false);
    }

    /**
     * method to set the view waiting for player to move.
     */
    @Override
    public void waitMove() {
        this.enable(this.undo);
        this.disable(this.diceRoll);
    }

    /**
     * method to set the view waiting for player to select character.
     */
    @Override
    public void waitSelect() {
        this.disable(this.diceRoll);
        this.disable(this.undo);
        this.lblDice.setVisible(true);
    }

    // public static void main(final String[] args) {
    // JFrame frame = new JFrame();
    // GameView pa = new GameView();
    // frame.getContentPane().add(pa);
    // frame.pack();
    // frame.setVisible(true);
    // }

}
