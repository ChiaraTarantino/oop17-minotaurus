package view.board;

import utilities.Colors;

public interface GameViewInterface {

    void waitSelect();

    void waitMove();

    void waitDice();

    void setTurnPlayerText(String player);

    void setInstructionsText(String instruction);

    void setImagePath(String resultPath);

    void setHeroArrived(Integer arrived, Integer requested);

    void setDiceImage(String imagePath);

    void setAvailableNStepsText(Integer nSteps);

    void repaintLabel(Colors color);

    void redrawLabel(String avviso);

    void setAudioText(String text);

    void setState(AudioState state);

}
