package view.board;

public interface AudioState {

    abstract public void execute(GameView gameview);

}
