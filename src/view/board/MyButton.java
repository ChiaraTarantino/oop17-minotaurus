package view.board;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JButton;

public class MyButton extends JButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final int TITLEFONT = 17;

    MyButton(String text) {
        super();
        this.setText(text);
        this.setForeground(SystemColor.text);
        this.setFont(new Font("Tahoma", Font.BOLD, TITLEFONT));
        this.setBackground(Color.BLACK);
        this.setForeground(Color.WHITE);
    }

}
