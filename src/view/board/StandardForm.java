package view.board;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JLabel;

public abstract class StandardForm {

    private static final int TITLEFONT = 16;
    private static final int CONTENTFONT = 15;

    public void applyButtonStyle(final JButton button) {
        button.setForeground(SystemColor.text);
        button.setFont(new Font("Tahoma", Font.BOLD, TITLEFONT));
        button.setBackground(Color.BLACK);
        button.setForeground(Color.WHITE);
    }

    private void applyContentStyle(final JLabel label) {
        label.setForeground(Color.BLACK);
        label.setFont(new Font("Tahoma", Font.PLAIN, CONTENTFONT));
    }

    public void applyLabelStyle(final JLabel label) {
        label.setForeground(Color.BLACK);
        label.setFont(new Font("Tahoma", Font.BOLD, TITLEFONT));
    }

}
