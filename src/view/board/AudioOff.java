package view.board;

import controller.audio.AudioManager;
import controller.audio.AudioManagerImpl;

public class AudioOff implements AudioState {

    private static final String AUDIOON = "AUDIO: ON";
    private final AudioManager audioManager = new AudioManagerImpl();

    @Override
    public void execute(GameView gameview) {
        audioManager.setAudio(true);
        gameview.setAudioText(AUDIOON);
        gameview.setState(new AudioOn());
    }

}
