package view.scenecontroller;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import view.utilities.Scenes;

public class RankingController extends SceneControllerImpl {
    
    //private Controller controller = ControllerImpl.getLog();
        
    @FXML
    private TextArea area;
    
    @FXML
    private Button back;
    
    @FXML
    private void initialize() {
        String prova = new String();
        prova=this.getController().getRanking();
        prova=prova.substring(29);
        area.appendText(prova);
    }
    
    @FXML
    private void returnStartMenu() throws IOException {
        this.getSceneLoader().load(Scenes.STARTMENU);
    }
}
